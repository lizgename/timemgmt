/*
 * Copyright (C) 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.example.timemgmt;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Simple notes database access helper class. Defines the basic CRUD operations
 * for the notepad example, and gives the ability to list all notes as well as
 * retrieve or modify a specific note.
 * 
 * This has been improved from the first version of this tutorial through the
 * addition of better error handling and also using returning a Cursor instead
 * of using a collection of inner classes (which is less scalable and not
 * recommended).
 */
public class DbAdapter {

	public static final String KEY_TITLE = "title";
	public static final String KEY_BODY = "body";
	public static final String KEY_ROWID = "_id";
	public static final String KEY_TASK_NAME = "task_name";
	public static final String KEY_TYPE = "type";
	public static final String KEY_PRECONDITION = "precondition";
	public static final String KEY_IS_FINISH = "is_finish";
	public static final String KEY_PER_COMPLETE = "per_complete";
	public static final String KEY_START_TIME = "start_time";
	public static final String KEY_END_TIME = "end_time";
	public static final String KEY_CONTENT = "content";
	public static final String KEY_PARTICIPANT = "participant";
	public static final String KEY_CREATOR = "creator";
	public static final String KEY_CREATE_TIME = "create_time";
	public static final String KEY_MODIFIER = "modifier";
	public static final String KEY_MODIFY_TIME = "modify_time";
	public static final String KEY_IS_ALERT = "is_alert";
	public static final String KEY_ALERT_TIME = "alert_time";
	public static final String KEY_IS_SEND_ALERT = "is_send_alert";
	public static final String KEY_IS_DO_ALERT = "is_do_alert";
	
	
	private static final String TAG = "TimeMgmtDbAdapter";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	/**
	 * Database creation sql statement
	 */
	private static final String DATABASE_CREATE = "create table tasks (_id integer primary key autoincrement, "
			+ "task_name TEXT NOT NULL,    type INTEGER NOT NULL,    precondition TEXT,    is_finish INTEGER,    per_complete INTEGER,    start_time TEXT,    end_time TEXT,    content TEXT,    participant TEXT,    creator TEXT,    create_time TEXT,    modifier TEXT,    modify_time TEXT,is_alert INTEGER,alert_time TEXT,is_send_alert INTEGER,is_do_alert INTEGER);";

	private static final String DATABASE_NAME = "TimeMgmtDB";
	private static final String TABLE_TASKS = "tasks";
	private static final int DATABASE_VERSION = 2;

	private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS notes");
			onCreate(db);
		}
	}

	/**
	 * Constructor - takes the context to allow the database to be
	 * opened/created
	 * 
	 * @param ctx
	 *            the Context within which to work
	 */
	public DbAdapter(Context ctx) {
		this.mCtx = ctx;
	}

	/**
	 * Open the notes database. If it cannot be opened, try to create a new
	 * instance of the database. If it cannot be created, throw an exception to
	 * signal the failure
	 * 
	 * @return this (self reference, allowing this to be chained in an
	 *         initialization call)
	 * @throws SQLException
	 *             if the database could be neither opened or created
	 */
	public DbAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	/**
	 * Create a new note using the title and body provided. If the note is
	 * successfully created return the new rowId for that note, otherwise return
	 * a -1 to indicate failure.
	 * 
	 * @param title
	 *            the title of the note
	 * @param body
	 *            the body of the note
	 * @return rowId or -1 if failed
	 */
	public long createNote(String title, String body) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_TITLE, title);
		initialValues.put(KEY_BODY, body);

		return mDb.insert(TABLE_TASKS, null, initialValues);
	}

	/**
	 * Create a new note using the title and body provided. If the note is
	 * successfully created return the new rowId for that note, otherwise return
	 * a -1 to indicate failure.
	 * 
	 * public static final String KEY_TITLE = "title"; public static final
	 * String KEY_BODY = "body"; public static final String KEY_ROWID = "_id";
	 * public static final String KEY_TASK_NAME = "task_name"; public static
	 * final String KEY_TYPE = "type"; public static final String
	 * KEY_PRECONDITION = "precondition"; public static final String
	 * KEY_IS_FINISH = "is_finish"; public static final String KEY_PER_COMPLETE
	 * = "per_complete"; public static final String KEY_START_TIME =
	 * "start_time"; public static final String KEY_END_TIME = "end_time";
	 * public static final String KEY_CONTENT = "content"; public static final
	 * String KEY_PARTICIPANT = "participant"; public static final String
	 * KEY_CREATOR = "creator"; public static final String KEY_CREATE_TIME =
	 * "create_time"; public static final String KEY_MODIFIER = "modifier";
	 * public static final String KEY_MODIFY_TIME = "modify_time";
	 * 
	 * Map<String, Object> map = new HashMap<String, Object>();
	 * 
	 * @param title
	 *            the title of the note
	 * @param body
	 *            the body of the note
	 * @return rowId or -1 if failed
	 */
	public long createTask(Map<String, Object> task) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_TASK_NAME, task.get("task_name").toString());
		initialValues.put(KEY_TYPE, task.get("type").toString());
//		initialValues
//				.put(KEY_PRECONDITION, task.get("precondition").toString());
		initialValues.put(KEY_IS_FINISH, task.get("is_finish").toString());
		initialValues
				.put(KEY_PER_COMPLETE, task.get("per_complete").toString());
		initialValues.put(KEY_START_TIME, task.get("start_time").toString());
		initialValues.put(KEY_END_TIME, task.get("end_time").toString());
		initialValues.put(KEY_CONTENT, task.get("content").toString());
		initialValues.put(KEY_PARTICIPANT, task.get("participant").toString());
		initialValues.put(KEY_CREATE_TIME, task.get("create_time").toString());
		initialValues.put(KEY_MODIFIER, task.get("modifier").toString());
		initialValues.put(KEY_MODIFY_TIME, task.get("modify_time").toString());
		initialValues.put(KEY_IS_ALERT, task.get("is_alert").toString());
		initialValues.put(KEY_ALERT_TIME, task.get("alert_time").toString());
		// initialValues.put(KEY_BODY, body);

		return mDb.insert(TABLE_TASKS, null, initialValues);
	}

	/**
	 * Delete the note with the given rowId
	 * 
	 * @param rowId
	 *            id of note to delete
	 * @return true if deleted, false otherwise
	 */

	public boolean deleteTask(long rowId) {

		return mDb.delete(TABLE_TASKS, KEY_ROWID + "=" + rowId, null) > 0;
	}

	/**
	 * Return a Cursor over the list of all notes in the database
	 * 
	 * @return Cursor over all notes
	 */
/*	public Cursor fetchAllNotes() {

		return mDb.query(TABLE_TASKS, new String[] { KEY_ROWID, KEY_TITLE,
				KEY_BODY }, null, null, null, null, null);
	}*/

	public Cursor fetchAllTasks() {

		return mDb.query(TABLE_TASKS, new String[] { KEY_ROWID, KEY_TASK_NAME,
				KEY_CONTENT }, null, null, null, null, "_id desc");
	}

	public Cursor fetchTasks_by_type(int type_id) {

		return mDb.query(TABLE_TASKS, new String[] { KEY_ROWID, KEY_TASK_NAME,
				KEY_CONTENT }, "type=" + type_id + " and is_finish=0", null,
				null, null, null);
	}
	
	public Cursor fetchAlertTasks() {

/*		return mDb.query(TABLE_TASKS, new String[] { KEY_ROWID, KEY_TASK_NAME,
				KEY_CONTENT,KEY_ALERT_TIME },  "is_send_alert<>1 AND is_finish=0 AND is_alert=1", null, null, null, "_id desc");*/
		
		String table = "tasks";
		String[] columns =new String[] { KEY_ROWID, KEY_TASK_NAME,KEY_CONTENT,KEY_ALERT_TIME };
		String selection = "is_alert=1 and is_finish<>1";
//		String selection = "is_send_alert=0 and is_finish=0 and is_alert=1";
//		String[] selectionArgs = new String[]{"1","0","1"};
		String[] selectionArgs = null;
		String groupBy = null;
		String having = null;
		String orderBy = "_id desc";
		Cursor c = mDb.query(table, columns, selection, selectionArgs, groupBy, having, orderBy, null); 
		return c;
		
		/*return mDb.query(TABLE_TASKS, new String[] { KEY_ROWID, KEY_TASK_NAME,
				KEY_CONTENT }, null, null, null, null, "_id desc");*/
	}


	/**
	 * Return a Cursor positioned at the note that matches the given rowId
	 * 
	 * @param rowId
	 *            id of note to retrieve
	 * @return Cursor positioned to matching note, if found
	 * @throws SQLException
	 *             if note could not be found/retrieved
	 */
	public Cursor fetchNote(long rowId) throws SQLException {

		Cursor mCursor =

		mDb.query(true, TABLE_TASKS, new String[] { KEY_ROWID, KEY_TITLE,
				KEY_BODY }, KEY_ROWID + "=" + rowId, null, null, null, null,
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;

	}

	public Cursor fetchTask(long rowId) throws SQLException {

		Cursor mCursor =
		// query(boolean distinct, String table, String[] columns, String
		// selection, String[] selectionArgs, String groupBy, String having,
		// String orderBy, String limit)
		// Query the given URL, returning a Cursor over the result set.
		mDb.query(true, TABLE_TASKS, new String[] { KEY_ROWID, KEY_TASK_NAME,
				KEY_TYPE, KEY_PRECONDITION, KEY_IS_FINISH, KEY_PER_COMPLETE,
				KEY_START_TIME, KEY_END_TIME, KEY_CONTENT,KEY_IS_ALERT,KEY_ALERT_TIME }, KEY_ROWID + "="
				+ rowId, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
		/*
		 * public static final String KEY_TITLE = "title"; public static final
		 * String KEY_BODY = "body"; public static final String KEY_ROWID =
		 * "_id"; public static final String KEY_TASK_NAME = "task_name"; public
		 * static final String KEY_TYPE = "type"; public static final String
		 * KEY_PRECONDITION = "precondition"; public static final String
		 * KEY_IS_FINISH = "is_finish"; public static final String
		 * KEY_PER_COMPLETE = "per_complete"; public static final String
		 * KEY_START_TIME = "start_time"; public static final String
		 * KEY_END_TIME = "end_time"; public static final String KEY_CONTENT =
		 * "content"; public static final String KEY_PARTICIPANT =
		 * "participant"; public static final String KEY_CREATOR = "creator";
		 * public static final String KEY_CREATE_TIME = "create_time"; public
		 * static final String KEY_MODIFIER = "modifier"; public static final
		 * String KEY_MODIFY_TIME = "modify_time"; public static final String
		 * KEY_ALERT = "alert";
		 */
	}

	/**
	 * Update the note using the details provided. The note to be updated is
	 * specified using the rowId, and it is altered to use the title and body
	 * values passed in
	 * 
	 * @param rowId
	 *            id of note to update
	 * @param title
	 *            value to set note title to
	 * @param body
	 *            value to set note body to
	 * @return true if the note was successfully updated, false otherwise
	 */
	public boolean updateNote(long rowId, String title, String body) {
		ContentValues args = new ContentValues();
		args.put(KEY_TITLE, title);
		args.put(KEY_BODY, body);

		return mDb.update(TABLE_TASKS, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	/*
	 * public static final String KEY_BODY = "body"; public static final String
	 * KEY_ROWID = "_id"; public static final String KEY_TASK_NAME =
	 * "task_name"; public static final String KEY_TYPE = "type"; public static
	 * final String KEY_PRECONDITION = "precondition"; public static final
	 * String KEY_IS_FINISH = "is_finish"; public static final String
	 * KEY_PER_COMPLETE = "per_complete"; public static final String
	 * KEY_START_TIME = "start_time"; public static final String KEY_END_TIME =
	 * "end_time"; public static final String KEY_CONTENT = "content"; public
	 * static final String KEY_PARTICIPANT = "participant"; public static final
	 * String KEY_CREATOR = "creator"; public static final String
	 * KEY_CREATE_TIME = "create_time"; public static final String KEY_MODIFIER
	 * = "modifier"; public static final String KEY_MODIFY_TIME = "modify_time";
	 */
	public boolean updateTask(long rowId, Map<String, Object> task) {
		ContentValues args = new ContentValues();
		args.put(KEY_TASK_NAME, task.get("task_name").toString());
		args.put(KEY_TYPE, task.get("type").toString());
//		args.put(KEY_PRECONDITION, task.get("precondition").toString());
		args.put(KEY_IS_FINISH, task.get("is_finish").toString());
		args.put(KEY_PER_COMPLETE, task.get("per_complete").toString());
		args.put(KEY_START_TIME, task.get("start_time").toString());
		args.put(KEY_END_TIME, task.get("end_time").toString());
		args.put(KEY_CONTENT, task.get("content").toString());
		args.put(KEY_PARTICIPANT, task.get("participant").toString());
		args.put(KEY_CREATOR, task.get("creator").toString());
		args.put(KEY_CREATE_TIME, task.get("create_time").toString());
		args.put(KEY_MODIFIER, task.get("modifier").toString());
		args.put(KEY_MODIFY_TIME, task.get("modify_time").toString());
		
		args.put(KEY_IS_ALERT, task.get("is_alert").toString());
		args.put(KEY_ALERT_TIME, task.get("alert_time").toString());
		args.put(KEY_IS_SEND_ALERT, task.get("is_send_alert").toString());
		args.put(KEY_IS_DO_ALERT, task.get("is_do_alert").toString());
		return mDb.update(TABLE_TASKS, args, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public boolean update_is_send_alert(long rowId) {
		ContentValues args = new ContentValues();				
		args.put(KEY_IS_ALERT,"1" );		
		return mDb.update(TABLE_TASKS, args, KEY_ROWID + "=" + rowId, null) > 0;
	}
	
	public boolean update_is_do_alert(long rowId) {
		ContentValues args = new ContentValues();				
		args.put(KEY_IS_DO_ALERT,"1" );		
		return mDb.update(TABLE_TASKS, args, KEY_ROWID + "=" + rowId, null) > 0;
	}
	
	public SQLiteDatabase get_db() {
		return mDb;
	}
}
