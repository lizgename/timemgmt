package com.example.timemgmt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.SystemClock;

public class Timer {
	private static boolean is_start;
	private static AlarmManager am;
	private static Context main_context;
	private static DbAdapter mDbHelper;

	public static void StartTimer(Context context, AlarmManager alarmMng) {
		am = alarmMng;
		main_context = context;
		is_start = true;
		// create a PendingIntent that will perform a broadcast
		/*
		 * Intent receive_Intent = new Intent(context, MyReceiver.class);
		 * receive_Intent.putExtra("alert_task_id", "2"); // PendingIntent pi=
		 * PendingIntent.getBroadcast(context, 0, new //
		 * Intent(context,MyReceiver.class), 0); PendingIntent pi =
		 * PendingIntent.getBroadcast(context, 0, receive_Intent, 0);
		 */

		if (is_start) {
			mDbHelper = new DbAdapter(main_context);
			mDbHelper.open();
//			 { KEY_ROWID, KEY_TASK_NAME,KEY_CONTENT,KEY_ALERT_TIME };
			Cursor cur = mDbHelper.fetchAlertTasks();
//			cur.moveToFirst();
			while (cur.moveToNext()) {
				// 光标移动成功
				// 把数据取出
				// 访问 Cursor 的下标获得其中的数据
				int nameColumnIndex = cur.getColumnIndex(mDbHelper.KEY_ROWID);
				String get_task_id = cur.getString(nameColumnIndex);
				int nameColumnIndex2 = cur
						.getColumnIndex(mDbHelper.KEY_ALERT_TIME);
				String get_task_alert_time = cur.getString(nameColumnIndex2);
				Map<String, Object> alert_task = new HashMap<String, Object>();
				int nameColumnIndex3 = cur.getColumnIndex(mDbHelper.KEY_TASK_NAME);
				String get_task_name=cur.getString(nameColumnIndex3);
				alert_task.put("task_alert_time", get_task_alert_time);
				alert_task.put("task_id", get_task_id);
				alert_task.put("task_name", get_task_name);
				
				set_a_timer(alert_task);

			}
		/*	for (int i = 1; i <cur.getCount(); i++) {
				int nameColumnIndex = cur.getColumnIndex(mDbHelper.KEY_ROWID);
				String get_task_id = cur.getString(nameColumnIndex);
				int nameColumnIndex2 = cur
						.getColumnIndex(mDbHelper.KEY_ALERT_TIME);
				String get_task_alert_time = cur.getString(nameColumnIndex2);
				set_a_timer(get_task_alert_time, get_task_id);	
				cur.moveToNext();
			}*/
			
			is_start = false;
			/*
			 * // just use current time as the Alarm time. Calendar c =
			 * Calendar.getInstance(); c.set(Calendar.YEAR, 2013);
			 * c.set(Calendar.MONTH, Calendar.APRIL);// 也可以填数字，0-11,一月为0
			 * c.set(Calendar.DAY_OF_MONTH, 11); c.set(Calendar.HOUR_OF_DAY,
			 * 21); c.set(Calendar.MINUTE, 34); c.set(Calendar.SECOND, 0); //
			 * 设定时间为 2011年6月28日19点50分0秒 // c.set(2011, 05,28, 19,50, 0); //
			 * 也可以写在一行里 // schedule an alarm am.set(AlarmManager.RTC_WAKEUP,
			 * c.getTimeInMillis(), pi); is_start = false; //
			 * 时间到时，执行PendingIntent，只执行一次 //
			 * AlarmManager.RTC_WAKEUP休眠时会运行，如果是AlarmManager.RTC,在休眠时不会运行 //
			 * am.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), //
			 * 10000, pi); // 如果需要重复执行，使用上面一行的setRepeating方法，倒数第二参数为间隔时间,单位为毫秒
			 */
		} else {
			// cancel current alarm
//			 am.cancel(pi);
		}
	}

	public static void set_a_timer(Map<String, Object> alert_task) {
		String alert_datetime=alert_task.get("task_alert_time").toString();
		String alert_task_id=alert_task.get("task_id").toString();
		String alert_task_name=alert_task.get("task_name").toString();
		Intent receive_Intent = new Intent(main_context, MyReceiver.class);
		receive_Intent.putExtra("alert_task_id", alert_task_id);
		receive_Intent.putExtra("alert_task_name", alert_task_name);
		// PendingIntent pi= PendingIntent.getBroadcast(context, 0, new
		// Intent(context,MyReceiver.class), 0);
		PendingIntent pi = PendingIntent.getBroadcast(main_context, 0,
				receive_Intent,PendingIntent.FLAG_UPDATE_CURRENT);

		Calendar c = string_to_Calendar(alert_datetime, "yyyy-MM-dd HH:mm");
//		int triggerAtTime = (int) (SystemClock.elapsedRealtime() + 500000 * 1000);  
//		am.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pi);
		am.set(AlarmManager.RTC_WAKEUP,  c.getTimeInMillis(), pi);
		
	}

	public static Calendar string_to_Calendar(String date_time,
			String DateFormat) {
		String re_time = null;
		Calendar c = Calendar.getInstance();
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
		Date d;
		try {
			d = sdf.parse(date_time);
			long l = d.getTime();
			c.set(Calendar.YEAR, d.getYear());
			c.set(Calendar.MONTH, d.getMonth());// 也可以填数字，0-11,一月为0
			c.set(Calendar.DAY_OF_MONTH, d.getDay());
			c.set(Calendar.HOUR_OF_DAY, d.getHours());
			c.set(Calendar.MINUTE, d.getMinutes());
			c.set(Calendar.SECOND, 0);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c;
	}

	public static void cancel_alert(String alert_task_id) {
		Intent receive_Intent = new Intent(main_context, MyReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(main_context, 0,
				receive_Intent, 0);

		am.cancel(pi);
	}
}
