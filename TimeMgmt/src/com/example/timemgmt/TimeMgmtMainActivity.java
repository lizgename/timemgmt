package com.example.timemgmt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Tab页面手势滑动切换以及动画效果
 * 
 * @author D.Winter
 * 
 */
public class TimeMgmtMainActivity extends Activity {
	private DbAdapter mDbHelper;
	private static final int TASK_CREATE = 0;
	private static final int TASK_EDIT = 1;

	private static final int INSERT_ID = Menu.FIRST;
	private static final int DELETE_ID = Menu.FIRST + 1;

	// ViewPager是google SDk中自带的一个附加包的一个类，可以用来实现屏幕间的切换。
	private ViewPager mPager;// 页卡内容
	private List<View> listViews; // Tab页面列表
	private ImageView cursor;// 动画图片
	private TextView t1, t2, t3, t4, t5;// 页卡头标
	private int offset = 0;// 动画图片偏移量
	private int currIndex = 0;// 当前页卡编号
	private int bmpW;// 动画图片宽度

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		InitImageView();
		InitTextView();
		InitViewPager();

		// -----DB--------
		mDbHelper = new DbAdapter(this);
		mDbHelper.open();	
		
		//------ db dump----	
		SimpleDateFormat   formatter   =   new   SimpleDateFormat   ("yyyy_MM_dd_HH_mm_ss");   
		 Date   curDate   =   new   Date(System.currentTimeMillis());//获取当前时间   
		String   str   =   formatter.format(curDate);   
		DatabaseDump db_dump_xml=new DatabaseDump(mDbHelper.get_db(), "frank_db_dump_"+str+".xml",TimeMgmtMainActivity.this);
		db_dump_xml.exportData();
		
		//-------notification----------
		Notification_Control.Create_Main_Notification(TimeMgmtMainActivity.this);
				
		//----------------- alert----------------------
		AlarmManager am= (AlarmManager) getSystemService(ALARM_SERVICE );
		Timer.StartTimer(TimeMgmtMainActivity.this,am);

	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private void fillData_tab_by_tab_number(int tab_number) {
		Cursor tasksCursor;
		int  layout_task_row=R.layout.tasks_row;
		switch (tab_number) {
		case 5:
			tasksCursor = mDbHelper.fetchAllTasks();
			break;
		default:
			tasksCursor = mDbHelper.fetchTasks_by_type(tab_number);
			break;
		}
		switch (tab_number) {
		case 1:
			layout_task_row=R.layout.tab_1_tasks_row;
			break;
		case 2:
			layout_task_row=R.layout.tab_2_tasks_row;
			break;
		case 3:
			layout_task_row=R.layout.tab_3_tasks_row;
			break;
		case 4:
			layout_task_row=R.layout.tab_4_tasks_row;
			break;
		case 5:
			layout_task_row=R.layout.tab_5_tasks_row;
			break;
		default:			
			break;
		}

		startManagingCursor(tasksCursor);

		// Create an array to specify the fields we want to display in the list
		String[] from = new String[] { DbAdapter.KEY_TASK_NAME };

		// and an array of the fields we want to bind those fields to (in this
		int[] to = new int[] { R.id.text1 };
        
		// Now create a simple cursor adapter and set it to display
		SimpleCursorAdapter tasks = new SimpleCursorAdapter(this,
				layout_task_row, tasksCursor, from, to);
		
		int lv_tab_id;
		lv_tab_id=0;
		// R.id.lv_tab2;
		switch (tab_number) {
		case 1:
			lv_tab_id=R.id.lv_tab1;
			break;
		case 2:
			lv_tab_id=R.id.lv_tab2;
			break;
		case 3:
			lv_tab_id=R.id.lv_tab3;
			break;
		case 4:
			lv_tab_id=R.id.lv_tab4;
			break;
		case 5:
			lv_tab_id=R.id.lv_tab5;
			break;
		default:
			break;
		}
		ListView get_tab_lv = (ListView) findViewById(lv_tab_id);
		get_tab_lv.setAdapter(tasks);
		
		// add temClickListener
		get_tab_lv
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// 于对选中的项进行处理
						onListItemClick(arg0, arg1, arg2, arg3);
					}
				});
				
		// long click popup menu
		get_tab_lv.setOnCreateContextMenuListener(this);
		

	}

	/**
	 * 初始化头标
	 */
	private void InitTextView() {
		t1 = (TextView) findViewById(R.id.text1);
		t1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 25);
		t2 = (TextView) findViewById(R.id.text2);
		t2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 25);
		t3 = (TextView) findViewById(R.id.text3);
		t3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 25);
		t4 = (TextView) findViewById(R.id.text4);
		t4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 25);
		t5 = (TextView) findViewById(R.id.text5);
		t5.setTextSize(TypedValue.COMPLEX_UNIT_PX, 25);

		t1.setOnClickListener(new MyOnClickListener(0));
		t2.setOnClickListener(new MyOnClickListener(1));
		t3.setOnClickListener(new MyOnClickListener(2));
		t4.setOnClickListener(new MyOnClickListener(3));
		t5.setOnClickListener(new MyOnClickListener(4));
	}

	/**
	 * 初始化ViewPager
	 */
	private void InitViewPager() {
		mPager = (ViewPager) findViewById(R.id.vPager);
		listViews = new ArrayList<View>();
		LayoutInflater mInflater = getLayoutInflater();
		listViews.add(mInflater.inflate(R.layout.lay1, null));
		listViews.add(mInflater.inflate(R.layout.lay2, null));
		listViews.add(mInflater.inflate(R.layout.lay3, null));
		listViews.add(mInflater.inflate(R.layout.lay4, null));
		listViews.add(mInflater.inflate(R.layout.lay5, null));
		mPager.setAdapter(new MyPagerAdapter(listViews));
		mPager.setCurrentItem(0);
		mPager.setOnPageChangeListener(new MyOnPageChangeListener());

	}

	/**
	 * 初始化动画
	 */
	private void InitImageView() {
		cursor = (ImageView) findViewById(R.id.cursor);
		bmpW = BitmapFactory.decodeResource(getResources(), R.drawable.a)
				.getWidth();// 获取图片宽度
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int screenW = dm.widthPixels;// 获取分辨率宽度
		offset = (screenW / 5 - bmpW) / 2;// 计算偏移量
		Matrix matrix = new Matrix();
		matrix.postTranslate(offset, 0);
		cursor.setImageMatrix(matrix);// 设置动画初始位置
	}

	/**
	 * ViewPager适配器
	 */
	public class MyPagerAdapter extends PagerAdapter {
		public List<View> mListViews;

		public MyPagerAdapter(List<View> mListViews) {
			this.mListViews = mListViews;
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(mListViews.get(arg1));
		}

		@Override
		public void finishUpdate(View arg0) {
			int tab_number=currIndex+1;
			fillData_tab_by_tab_number(tab_number);
		}

		@Override
		public int getCount() {
			return mListViews.size();
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(mListViews.get(arg1), 0);
			return mListViews.get(arg1);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == (arg1);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
		}
	}

	/**
	 * 头标点击监听
	 */
	public class MyOnClickListener implements View.OnClickListener {
		private int index = 0;

		public MyOnClickListener(int i) {
			index = i;
		}

		@Override
		public void onClick(View v) {
			mPager.setCurrentItem(index);
		}
	};

	/**
	 * 页卡切换监听
	 */
	public class MyOnPageChangeListener implements OnPageChangeListener {

		int one = offset * 2 + bmpW;// 页卡1 -> 页卡2 偏移量
		int two = one * 2;// 页卡1 -> 页卡3 偏移量
		int three = one * 3;// 页卡1 -> 页卡4 偏移量
		int four = one * 4;// 页卡1 -> 页卡5偏移量

		@Override
		public void onPageSelected(int arg0) {
			Animation animation = null;
			switch (arg0) {
			case 0:
				if (currIndex == 1) {
					animation = new TranslateAnimation(one, 0, 0, 0);
				} else if (currIndex == 2) {
					animation = new TranslateAnimation(two, 0, 0, 0);
				} else if (currIndex == 3) {
					animation = new TranslateAnimation(three, 0, 0, 0);
				} else if (currIndex == 4) {
					animation = new TranslateAnimation(four, 0, 0, 0);
				}
				fillData_tab_by_tab_number(1);
				break;
			case 1:
				if (currIndex == 0) {
					animation = new TranslateAnimation(0, one, 0, 0);
				} else if (currIndex == 2) {
					animation = new TranslateAnimation(two, one, 0, 0);
				} else if (currIndex == 3) {
					animation = new TranslateAnimation(three, one, 0, 0);
				} else if (currIndex == 4) {
					animation = new TranslateAnimation(four, one, 0, 0);
				}
				fillData_tab_by_tab_number(2);
				break;
			case 2:
				if (currIndex == 1) {
					animation = new TranslateAnimation(one, two, 0, 0);
				} else if (currIndex == 0) {
					animation = new TranslateAnimation(offset, two, 0, 0);
				} else if (currIndex == 3) {
					animation = new TranslateAnimation(three, two, 0, 0);
				} else if (currIndex == 4) {
					animation = new TranslateAnimation(four, two, 0, 0);
				}
				fillData_tab_by_tab_number(3);
				break;
			case 3:
				if (currIndex == 0) {
					animation = new TranslateAnimation(offset, three, 0, 0);
				} else if (currIndex == 1) {
					animation = new TranslateAnimation(one, three, 0, 0);
				} else if (currIndex == 2) {
					animation = new TranslateAnimation(two, three, 0, 0);
				} else if (currIndex == 4) {
					animation = new TranslateAnimation(four, three, 0, 0);
				}
				fillData_tab_by_tab_number(4);
				break;
			case 4:
				if (currIndex == 0) {
					animation = new TranslateAnimation(offset, four, 0, 0);
				} else if (currIndex == 1) {
					animation = new TranslateAnimation(one, four, 0, 0);
				} else if (currIndex == 2) {
					animation = new TranslateAnimation(two, four, 0, 0);
				} else if (currIndex == 3) {
					animation = new TranslateAnimation(three, four, 0, 0);
				}
				fillData_tab_by_tab_number(5);
				break;
			}
			currIndex = arg0;
			animation.setFillAfter(true);// True:图片停在动画结束位置
			animation.setDuration(300);
			cursor.startAnimation(animation);

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	}

	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		// case INSERT_ID:
		case R.id.add_task:
			createTask();
			return true;
		}

		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, DELETE_ID, 0, R.string.menu_delete);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case DELETE_ID:
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			mDbHelper.deleteTask(info.id);
			if (currIndex == 0) {
				fillData_tab_by_tab_number(1);
			}
			return true;
		}
		return super.onContextItemSelected(item);
	}

	private void createTask() {
		Intent i = new Intent(this, TaskEdit.class);
		i.putExtra("current_tab", currIndex);
		i.putExtra("is_add_task",true);
		startActivityForResult(i, TASK_CREATE);
	}

	protected void onListItemClick(AdapterView<?> arg0, View v, int position,
			long id) {
		// super.onListItemClick(l, v, position, id);
		Intent i = new Intent(this, TaskEdit.class);
		i.putExtra(DbAdapter.KEY_ROWID, id);
		startActivityForResult(i, TASK_EDIT);
	}
	
    @Override
    public void onLowMemory() {

            super.onLowMemory();        

            System.gc();

    }
    
  /*  @Override
    public void onDestory(){
        App app = (App) getApplication();//获取应用程序全局的实例引用
        app.activities.remove(this); //把当前Activity从集合中移除
}*/



}