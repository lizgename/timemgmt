package com.example.timemgmt;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class Notification_Control {
	private static Activity m_activity;
	private static int notification_id=2;

	public static void Create_Main_Notification(Activity main_activity) {
		m_activity = main_activity;
		Context context = main_activity.getApplicationContext();
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(context.getString(R.string.app_name))
				.setContentText(context.getString(R.string.hello_world));
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(context, TimeMgmtMainActivity.class);
		// resultIntent.putExtra("current_tab", currIndex);
		// resultIntent.putExtra("is_add_task",true);
		// The stack builder object will contain an artificial back stack for
		// the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(TimeMgmtMainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);

		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(1, mBuilder.build());
		
	}

	public static void Create_Notification(Context main_context, String task_id) {
		Context context = main_context;
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(context.getString(R.string.app_name))
//				.setContentText(context.getString(R.string.hello_world));
				.setContentText("任务"+task_id+"需要马上执行");
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(context, TaskEdit.class);
		resultIntent.putExtra("alert_task_id", task_id);
		resultIntent.putExtra("is_alert_task", true);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(TimeMgmtMainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		notification_id = notification_id + 1;
		mNotificationManager.notify(notification_id, mBuilder.build());
	}
}
