package com.example.timemgmt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

public class TaskEdit extends Activity {
	boolean b = false;
	private DbAdapter mDbHelper;
	private EditText mTaskNameText;
	private EditText mPreconditionText;
	private Spinner mSpinner_type;
	private EditText from_date;
	private EditText from_time;
	private EditText to_date;
	private EditText to_time;
	private EditText alert_date;
	private EditText alert_time;
	// private Spinner to_date;
	// private Spinner to_time;
	private CheckBox mIs_finish;
	private CheckBox mIs_alert;
	private EditText mPercent;
	// private EditText mStart_date_time;
	// private EditText mEnd_date_time;
	private EditText mContent;

	private Long mRowId;

	private int mYear;
	private int mMonth;
	private int mDay;
	private int h;
	private int m;
	private int s;
	private int current_tab;
	private Boolean is_add_task;
	private Boolean is_alert_task;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		h = c.get(Calendar.HOUR_OF_DAY);
		m = c.get(Calendar.MINUTE);
		s = c.get(Calendar.SECOND);
		
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.task_edit);
		setTitle(R.string.edit_task);
		
		String get_alert_task_id;
		long alert_task_id=1;
		Bundle extras1 = getIntent().getExtras(); 
		current_tab=extras1.getInt("current_tab");
		is_add_task=extras1.getBoolean("is_add_task");
		is_alert_task=extras1.getBoolean("is_alert_task");
		get_alert_task_id=extras1.getString("alert_task_id");
		if(get_alert_task_id!=null){
//		if(get_alert_task_id.length()>1){
			alert_task_id=Long.parseLong(get_alert_task_id);	
		}
		
//		mRowId=Long.parseLong(extras1.getString("task_id"));
		mDbHelper = new DbAdapter(this);
		mDbHelper.open();

		mTaskNameText = (EditText) findViewById(R.id.task_name);
		// ------spinner start-------
		Spinner spinner = (Spinner) findViewById(R.id.spinner_type);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.task_type_array,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setPrompt("please select:");
		// spinner.setPromptId(R.string.task_type_prompt);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
		// ------spinner end-------

		mSpinner_type = spinner;

//		mPreconditionText = (EditText) findViewById(R.id.precondition);
		mIs_finish = (CheckBox) findViewById(R.id.checkbox_is_finish);
		mIs_finish.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					mPercent.setText("100");
					update_End_Date_Display();
					update_End_Time_Display();
				} else {
					mPercent.setText("0");
				}
			}
		});
		
		mIs_alert = (CheckBox) findViewById(R.id.checkbox_is_alert);
		mIs_alert.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					alert_date.setVisibility(0);
					alert_time.setVisibility(0);
					update_alert_Date_Display();
					update_alert_Time_Display();
				} else {
					alert_date.setVisibility(4);
//					alert_date.setText("");
					alert_time.setVisibility(4);
//					alert_time.setText("");
				}
			}
		});
		alert_date = (EditText) findViewById(R.id.EditText_alert_date);
		alert_date.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				showDialog(5);
				return false;
			}
		});
		alert_time = (EditText) findViewById(R.id.EditText_alert_time);
		alert_time.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				showDialog(6);
				return false;
			}
		});


		mPercent = (EditText) findViewById(R.id.percent);
		// mStart_date_time = (EditText) findViewById(R.id.start_time);
		// mEnd_date_time = (EditText) findViewById(R.id.end_time);
		mContent = (EditText) findViewById(R.id.content);

		from_date = (EditText) findViewById(R.id.EditText_from_date);
		from_date.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				showDialog(0);
				return false;
			}
		});

		from_time = (EditText) findViewById(R.id.EditText_from_time);
		from_time.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				showDialog(1);
				return false;
			}
		});

		to_date = (EditText) findViewById(R.id.EditText_to_date);
		to_date.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				showDialog(2);
				return false;
			}
		});

		to_time = (EditText) findViewById(R.id.EditText_to_time);
		to_time.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				String get_to_date = getDate(to_date.getText().toString(),
						"yyyy-MM-dd");
				if (get_to_date==null) {
					/*Toast toast = Toast.makeText(getApplicationContext(),
							R.string.alert_txt_to_date_null,
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					to_date.requestFocus();*/
					showDialog(2);
					showDialog(3);
				} else {
					showDialog(3);
				}
				
				return false;
			}
		});		
	

		

		
		mRowId = (savedInstanceState == null) ? null
				: (Long) savedInstanceState
						.getSerializable(DbAdapter.KEY_ROWID);
		if (mRowId == null) {
			Bundle extras = getIntent().getExtras();
			if(is_add_task!=true)
			{mRowId = extras.getLong(DbAdapter.KEY_ROWID);}		
			
			if(is_alert_task==true)
			{mRowId = alert_task_id;}		
			
			/*mRowId = extras != null ? extras.getLong(DbAdapter.KEY_ROWID)
					: null;*/
		}
		
		// -----define confirmbutton---start-------
//		ImageButton confirmButton = (ImageButton) findViewById(R.id.imageButton_save);
		Button confirmButton = (Button) findViewById(R.id.confirm);
		confirmButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				String get_task_name = mTaskNameText.getText().toString();
				if (!get_task_name.isEmpty()) {
					setResult(RESULT_OK);
					finish();
				} else {
					// task name is null
					Toast toast = Toast.makeText(getApplicationContext(),
							R.string.alert_txt_task_name_null,
							Toast.LENGTH_LONG);
					toast.setGravity(Gravity.TOP, 0, 0);
					toast.show();
					mTaskNameText.requestFocus();
					// mTaskNameText.setFocusableInTouchMode(focusableInTouchMode);
				}
			}

		});
	
		
		// -----define confirmbutton---end-------
		
		// -----define cancel button---start-------
//		ImageButton cancel_Button = (ImageButton) findViewById(R.id.imageButton_cancel);
		Button cancel_Button = (Button) findViewById(R.id.cancel);
		cancel_Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {				
				TaskEdit.this.finish();
			}

		});
	/*	Button cancel_Button = (Button) findViewById(R.id.cancel);

		cancel_Button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				
				 * new AlertDialog.Builder(view.getContext()).setTitle("确认")
				 * .setMessage("确定吗？").setPositiveButton("是",
				 * click_yes_listener) .setNegativeButton("否", null).show();
				 
				AlertDialog.Builder builder = new AlertDialog.Builder(
						TaskEdit.this);
				builder.setMessage(R.string.message);
				builder.setTitle(R.string.title);
				builder.setPositiveButton(R.string.positive_button_string,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								TaskEdit.this.finish();
							}
						});
				builder.setNegativeButton(R.string.negative_button_string,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				builder.create().show();
				TaskEdit.this.finish();
			}

		});*/
		// -----define cancel_button---end-------
		populateFields();
	}

	/*
	 * public class from_date_click_Listener implements OnClickListener {
	 * 
	 * @Override public void onClick(View v) { showDialog(0);
	 * 
	 * } }
	 */

	private void update_End_Date_Display() {
		// TODO Auto-generated method stub
		to_date.setText(new StringBuilder().append(mYear).append("-")
				.append(formateTime(mMonth + 1)).append("-")
				.append(formateTime(mDay)));
	}

	private void update_End_Time_Display() {
		// TODO Auto-generated method stub
		to_time.setText(new StringBuilder().append(formateTime(h)).append(":")
				.append(formateTime(m)));
	}
	
	private void update_alert_Date_Display() {
		// TODO Auto-generated method stub
		alert_date.setText(new StringBuilder().append(mYear).append("-")
				.append(formateTime(mMonth + 1)).append("-")
				.append(formateTime(mDay)));
	}

	private void update_alert_Time_Display() {
		// TODO Auto-generated method stub
		alert_time.setText(new StringBuilder().append(formateTime(h)).append(":")
				.append(formateTime(m)));
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		case 1:
			return new TimePickerDialog(this, mtimesetListener, h, m, true);
		case 2:
			return new DatePickerDialog(this, mDateSetListener2, mYear, mMonth,
					mDay);
		case 3:
			return new TimePickerDialog(this, mtimesetListener2, h, m, true);
		case 5:
			return new DatePickerDialog(this, mDateSetListener3, mYear, mMonth,
					mDay);
		case 6:
			return new TimePickerDialog(this, mtimesetListener3, h, m, true);
		}
		return null;
	}

	private void update_Start_Date_Display() {
		/*
		 * mStart_date_time.setText(new
		 * StringBuilder().append(mYear).append("-") .append(formateTime(mMonth
		 * + 1)).append("-")
		 * .append(formateTime(mDay)).append(" ").append(formateTime(h))
		 * .append(":").append(formateTime(m)));
		 */
		from_date.setText(new StringBuilder().append(mYear).append("-")
				.append(formateTime(mMonth + 1)).append("-")
				.append(formateTime(mDay)));

	}	

	private void update_Start_Time_Display() {
		/*
		 * mStart_date_time.setText(new
		 * StringBuilder().append(mYear).append("-") .append(formateTime(mMonth
		 * + 1)).append("-")
		 * .append(formateTime(mDay)).append(" ").append(formateTime(h))
		 * .append(":").append(formateTime(m)));
		 */
		from_time.setText(new StringBuilder().append(formateTime(h))
				.append(":").append(formateTime(m)));

	}
	

	// 获得当前日期
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			update_Start_Date_Display();
		}
	};
	// 获得当前时间
	private TimePickerDialog.OnTimeSetListener mtimesetListener = new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			h = hourOfDay;
			m = minute;
			update_Start_Time_Display();
		}
	};

	// 获得当前日期
	private DatePickerDialog.OnDateSetListener mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			update_End_Date_Display();
		}
	};
	// 获得当前时间
	private TimePickerDialog.OnTimeSetListener mtimesetListener2 = new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			h = hourOfDay;
			m = minute;
			update_End_Time_Display();
		}
	};
	
	// 获得当前日期
	private DatePickerDialog.OnDateSetListener mDateSetListener3 = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			update_alert_Date_Display();
		}
	};
	// 获得当前时间
	private TimePickerDialog.OnTimeSetListener mtimesetListener3 = new TimePickerDialog.OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// TODO Auto-generated method stub
			h = hourOfDay;
			m = minute;
			update_alert_Time_Display();
		}
	};

	private String formateTime(int time) {

		String timeStr = "";
		if (time < 10) {
			timeStr = "0" + String.valueOf(time);
		} else {
			timeStr = String.valueOf(time);
		}
		return timeStr;
	}

	private void populateFields() {
		if (mRowId != null) {
			Cursor task = mDbHelper.fetchTask(mRowId);
			startManagingCursor(task);
			mTaskNameText.setText(task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_TASK_NAME)));
//			mPreconditionText.setText(task.getString(task
//					.getColumnIndexOrThrow(DbAdapter.KEY_PRECONDITION)));

			mSpinner_type.setSelection(Integer.valueOf(task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_TYPE))) - 1);

			if (Integer.valueOf(task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_IS_FINISH))) == 1)
				mIs_finish.setChecked(true);
			else
				mIs_finish.setChecked(false);

			mPercent.setText(task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_PER_COMPLETE)));

			String get_start_time = task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_START_TIME));
			from_date.setText(getDate(get_start_time, "yyyy-MM-dd HH:mm"));
			from_time.setText(getTime(get_start_time, "yyyy-MM-dd HH:mm"));

			String get_end_time = task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_END_TIME));
			if (get_end_time.length() < 2) {
				to_date.setText(R.string.end_date);
				to_time.setText(R.string.end_time);
			} else {
				to_date.setText(getDate(get_end_time, "yyyy-MM-dd HH:mm"));
				to_time.setText(getTime(get_end_time, "yyyy-MM-dd HH:mm"));
			}

			//-------------alert time
			if (Integer.valueOf(task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_IS_ALERT))) == 1)
				mIs_alert.setChecked(true);
			else{
				mIs_alert.setChecked(false);
				alert_date.setVisibility(4);
				alert_time.setVisibility(4);
			}
			String get_alert_time = task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_ALERT_TIME));
			alert_date.setText(getDate(get_alert_time, "yyyy-MM-dd HH:mm"));
			alert_time.setText(getTime(get_alert_time, "yyyy-MM-dd HH:mm"));

			/*
			 * mStart_date_time.setText(task.getString(task
			 * .getColumnIndexOrThrow(DbAdapter.KEY_START_TIME)));
			 * 
			 * mEnd_date_time.setText(task.getString(task
			 * .getColumnIndexOrThrow(DbAdapter.KEY_END_TIME)));
			 */

			mContent.setText(task.getString(task
					.getColumnIndexOrThrow(DbAdapter.KEY_CONTENT)));
//			task.close();
			/*
			 * private DbAdapter mDbHelper; private EditText mTaskNameText;
			 * private EditText mPreconditionText; private Spinner
			 * mSpinner_type; private CheckBox mIs_finish; private EditText
			 * mPercent; private EditText mStart_date_time; private EditText
			 * mEnd_date_time; private EditText mContent;
			 */
		} else {
			if (current_tab==4) {
				mSpinner_type.setSelection(0);
			} else {
				mSpinner_type.setSelection(current_tab);
			}
			
			update_Start_Date_Display();
			update_Start_Time_Display();
			alert_date.setVisibility(4);
			alert_time.setVisibility(4);
		}
	}

	public static String getTime(String user_time, String TimeFormat) {
		String re_time = null;
		SimpleDateFormat sdf = new SimpleDateFormat(TimeFormat);
		Date d;
		try {
			d = sdf.parse(user_time);
			long l = d.getTime();
			String str = String.valueOf(l);
			// re_time = str.substring(0, 10);
			re_time = getStrTime(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return re_time;
	}

	@SuppressLint("SimpleDateFormat")
	public static String getDate(String user_time, String DateFormat) {
		String re_time = null;
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat);
		Date d;
		try {
			d = sdf.parse(user_time);
			long l = d.getTime();
			String str = String.valueOf(l);
			re_time = str.substring(0, 10);
			re_time = getStrDate(re_time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return re_time;
	}

	// 将时间戳转为字符串
	@SuppressLint("SimpleDateFormat")
	public static String getStrTime(String cc_time) {
		String re_StrTime = null;

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		// 例如：cc_time=1291778220

		long lcc_time = Long.valueOf(cc_time);
		re_StrTime = sdf.format(lcc_time);

		return re_StrTime;

	}

	// 将时间戳转为字符串
	@SuppressLint("SimpleDateFormat")
	public static String getStrDate(String cc_time) {
		String re_StrTime = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		// 例如：cc_time=1291778220
		long lcc_time = Long.valueOf(cc_time);
		re_StrTime = sdf.format(new Date(lcc_time * 1000L));

		return re_StrTime;

	}
	

	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			/*
			 * Toast.makeText(parent.getContext(), "The task type is " +
			 * parent.getItemAtPosition(pos).toString(),
			 * Toast.LENGTH_LONG).show();
			 */
			// if(b){
			// parent.set
			// }
			// b=true;
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_task_menu, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		// case INSERT_ID:
		case R.id.delete_task:
			if (is_add_task != true) {
				
				AlertDialog.Builder builder = new AlertDialog.Builder(
						TaskEdit.this);
				builder.setMessage(R.string.message_delete);
				builder.setTitle(R.string.title);
				builder.setPositiveButton(R.string.positive_button_string,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								delete_task(mRowId);
								TaskEdit.this.finish();
							}
						});
				builder.setNegativeButton(R.string.negative_button_string,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				builder.create().show();
				
			} 			
			return true;
		}
		return true;
//		return super.onMenuItemSelected(featureId, item);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		saveState();
		outState.putSerializable(DbAdapter.KEY_ROWID, mRowId);
	}

	@Override
	protected void onPause() {
		super.onPause();
		saveState();
	}

	@Override
	protected void onResume() {
		super.onResume();
		populateFields();
	}
	
	private boolean delete_task(long task_id) {
		boolean delete_result = false;
		delete_result = mDbHelper.deleteTask(task_id);
		return delete_result;
	}

		
	private void saveState() {
		// String title = mTasskNameText.getText().toString();
		// String body = mPreconditionText.getText().toString();

		Map<String, Object> task = new HashMap<String, Object>();
		String get_task_name = mTaskNameText.getText().toString();
		if (get_task_name.equals(null) || get_task_name.equals("")) {

		} else {
			task.put("task_name", mTaskNameText.getText().toString());
			task.put("type", mSpinner_type.getSelectedItemPosition() + 1);
//			task.put("precondition", mPreconditionText.getText().toString());
			if (mIs_finish.isChecked()) {
				task.put("is_finish", "1");// 1 为true 0 为 false
			} else {
				task.put("is_finish", "0");// 1 为true 0 为 false
			}

			task.put("per_complete", mPercent.getText().toString());

			task.put("start_time", from_date.getText().toString() + " "
					+ from_time.getText().toString());

			String end_time = "";
			String get_to_date = getDate(to_date.getText().toString(),
					"yyyy-MM-dd");
			String get_to_time = getTime(to_time.getText().toString(), "HH:mm");
			/*if (get_to_date != null) {
				end_time = get_to_date;
			}*/
			if (get_to_time == null) {
				if (get_to_date != null) {
					end_time = get_to_date + " 00:00";
				}
			}else{				
				if (get_to_date != null) {
					end_time = get_to_date + " " + get_to_time;
				} 
			}
			// task.put("end_time",getDate(to_date.getText().toString())+" "+
			// getTime(to_time.getText().toString()));
			task.put("end_time", end_time);
			
//			---------------alert time---------------------- 
			if (mIs_alert.isChecked()) {
				task.put("is_alert", "1");// 1 为true 0 为 false
				task.put("is_send_alert", "0");
				task.put("is_do_alert", "0");
				String s_alert_time = "";
				String get_alert_date = getDate(alert_date.getText().toString(),
						"yyyy-MM-dd");
				String get_alert_time = getTime(alert_time.getText().toString(), "HH:mm");
				s_alert_time= get_alert_date + " " + get_alert_time;
				task.put("alert_time", s_alert_time);
			} else {
				task.put("is_alert", "0");// 1 为true 0 为 false
				task.put("is_send_alert", "0");
				task.put("is_do_alert", "0");
				/*String s_alert_time = "";
				String get_alert_date = getDate(alert_date.getText().toString(),
						"yyyy-MM-dd");
				String get_alert_time = getTime(alert_time.getText().toString(), "HH:mm");
				s_alert_time= get_alert_date + " " + get_alert_time;*/
				task.put("alert_time", "");
			}
			
			
			task.put("content", mContent.getText().toString());

			task.put("participant", "");

			set_current_time();
			StringBuilder current_time = new StringBuilder().append(mYear)
					.append("-").append(formateTime(mMonth + 1)).append("-")
					.append(formateTime(mDay)).append(" ")
					.append(formateTime(h)).append(":").append(formateTime(m))
					.append(":").append(formateTime(s));

			/*
			 * public static final String KEY_TASK_NAME = "task_name"; public
			 * static final String KEY_TYPE = "type"; public static final String
			 * KEY_PRECONDITION = "precondition"; public static final String
			 * KEY_IS_FINISH = "is_finish"; public static final String
			 * KEY_PER_COMPLETE = "per_complete"; public static final String
			 * KEY_START_TIME = "start_time"; public static final String
			 * KEY_END_TIME = "end_time"; public static final String KEY_CONTENT
			 * = "content"; public static final String KEY_PARTICIPANT =
			 * "participant"; public static final String KEY_CREATOR =
			 * "creator"; public static final String KEY_CREATE_TIME =
			 * "create_time"; public static final String KEY_MODIFIER =
			 * "modifier"; public static final String KEY_MODIFY_TIME =
			 * "modify_time"; public static final String KEY_ALERT = "alert";
			 */

			if (mRowId == null) {
				// long id = mDbHelper.createNote(title, body);
				task.put("create_time", current_time.toString());
				task.put("creator", "unregistered");
				task.put("modifier", "");
				task.put("modify_time", "");
				long id = mDbHelper.createTask(task);
				if (id > 0) {
					mRowId = id;
				}
			} else {
				// mDbHelper.updateNote(mRowId, title, body);
				task.put("create_time", current_time.toString());
				task.put("creator", "unregistered");
				task.put("modifier", "unregistered");
				task.put("modify_time", current_time.toString());
				mDbHelper.updateTask(mRowId, task);
			}
		}
	}

	private void set_current_time() {
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		h = c.get(Calendar.HOUR_OF_DAY);
		m = c.get(Calendar.MINUTE);
		s = c.get(Calendar.SECOND);
	}
}
