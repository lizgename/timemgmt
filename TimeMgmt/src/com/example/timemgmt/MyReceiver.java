package com.example.timemgmt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
* Receive the broadcast and start the activity that will show the alarm
*/
public class MyReceiver extends BroadcastReceiver {

    /**
     * called when the BroadcastReceiver is receiving an Intent broadcast.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        
        /* start another activity - MyAlarm to display the alarm */
    	
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setClass(context, MyAlarm.class);
//        context.startActivity(intent);
    	Bundle extras1 = intent.getExtras(); 
		String alert_task_id=extras1.getString("alert_task_id");
		String alert_task_name=extras1.getString("alert_task_name");
		
        Notification_Control.Create_Notification(context,alert_task_id+alert_task_name);
        
      //收到广播后启动Activity,简单起见，直接就跳到了设置alarm的Activity
        //intent必须加上Intent.FLAG_ACTIVITY_NEW_TASK flag
        
    }

}